package com.sky.phone.service.impl;

import com.sky.phone.component.PhoneRecordParser;
import com.sky.phone.component.PhoneRecordsReader;
import com.sky.phone.component.impl.PhoneRecordParserImpl;
import com.sky.phone.config.PhoneBookConfig;
import com.sky.phone.data.CustomerPhoneRecords;
import com.sky.phone.data.PhoneCall;
import com.sky.phone.data.PhoneRecord;
import com.sky.phone.service.PhoneRecordService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PhoneBookConfig.class)
public class PhoneRecordServiceImplTest {

    @Mock
    PhoneRecordsReader<Map<CharSequence, List<PhoneCall>>> phoneRecordsReader ;

    @InjectMocks
    private String fileNameDetails = "fileName";

    @Spy
    private PhoneRecordParser<Map<CharSequence, List<PhoneCall>>, CustomerPhoneRecords>
                                phoneRecordParser = new PhoneRecordParserImpl();

    @InjectMocks
    PhoneRecordService<CustomerPhoneRecords> phoneRecordService = new PhoneRecordServiceImpl();

    @BeforeEach
    public void setUp(){
    }

    @ParameterizedTest
    @MethodSource("phoneCallsRecords")
    public void testPublishPhoneRecords_thenCorrect(Map<CharSequence, List<PhoneCall>> records){
        final String expectedCustomerName = "A";
        final String expectedDuration = "30.89";
        when(phoneRecordsReader.parsePhoneRecords(null)).thenReturn(records);
        CustomerPhoneRecords expectedCustomerPhoneRecords = phoneRecordService.publishPhoneRecords();
        System.out.println("Customer records="+expectedCustomerPhoneRecords);
        assertThat(expectedCustomerPhoneRecords.getPhoneRecords().size(), is(1));
        PhoneRecord phoneRecord = expectedCustomerPhoneRecords.getPhoneRecords().get(0);
        assertThat(phoneRecord.getCustomerId(), is(expectedCustomerName));
        assertThat(phoneRecord.getDuration(), is(expectedDuration));
    }

    /**
     * Set of records for customer 'A'
     * ssssssss=A 555-333-212 00:02:03
     * ssssssss=A 555-433-242 00:06:41
     * ssssssss=A 555-433-242 00:01:03
     * ssssssss=A 555-333-212 00:01:10
     * ssssssss=A 555-663-111 00:02:09
     * ssssssss=A 555-333-212 00:04:28
     * @return
     */
    static public Stream<Arguments> phoneCallsRecords(){
        Map<CharSequence, List<PhoneCall>> records  = new HashMap<>();
        ArrayList<PhoneCall> phoneCalls = new ArrayList<>();
        PhoneCall.Builder builder = PhoneCall.newBuilder();
        builder.setCustomerId("A").setPhoneNumber("555-333-212").setDuration("00:02:03");
        phoneCalls.add(builder.build());
        builder.setCustomerId("A").setPhoneNumber("555-433-242").setDuration("00:06:41");
        phoneCalls.add(builder.build());
        builder.setCustomerId("A").setPhoneNumber("555-433-242").setDuration("00:01:03");
        phoneCalls.add(builder.build());
        builder.setCustomerId("A").setPhoneNumber("555-333-212").setDuration("00:01:10");
        phoneCalls.add(builder.build());
        builder.setCustomerId("A").setPhoneNumber("555-663-111").setDuration("00:02:09");
        phoneCalls.add(builder.build());
        builder.setCustomerId("A").setPhoneNumber("555-333-212").setDuration("00:04:28");
        phoneCalls.add(builder.build());
        records.put("A", phoneCalls);
        return Stream.of(Arguments.of(records));
    }
}