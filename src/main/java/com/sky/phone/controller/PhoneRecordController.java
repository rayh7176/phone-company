package com.sky.phone.controller;

import com.sky.phone.data.CustomerPhoneRecords;
import com.sky.phone.service.PhoneRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("phone")
public class PhoneRecordController {

    @Autowired
    private PhoneRecordService<CustomerPhoneRecords> phoneRecordService;

    @GetMapping(value="records" , produces= { "application/avro+json" })
    public CustomerPhoneRecords phoneRecords() {
        return phoneRecordService.publishPhoneRecords();
    }

}
