package com.sky.phone.service.impl;

import com.sky.phone.component.PhoneRecordParser;
import com.sky.phone.component.PhoneRecordsReader;
import com.sky.phone.data.CustomerPhoneRecords;
import com.sky.phone.data.PhoneCall;
import com.sky.phone.service.PhoneRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@Qualifier("PhoneRecordService")
public class PhoneRecordServiceImpl implements PhoneRecordService<CustomerPhoneRecords> {

    @Autowired
    PhoneRecordsReader<Map<CharSequence, List<PhoneCall>>> phoneRecordsReader;

    @Autowired
    private String fileNameDetails;

    @Autowired
    PhoneRecordParser<Map<CharSequence, List<PhoneCall>>, CustomerPhoneRecords> phoneRecordParser;

    @Override
    public CustomerPhoneRecords publishPhoneRecords() {
        // read records
        Map<CharSequence, List<PhoneCall>> records =  phoneRecordsReader.parsePhoneRecords(fileNameDetails);
        // process records
        return phoneRecordParser.sumPhoneRecords(records);
    }
}
