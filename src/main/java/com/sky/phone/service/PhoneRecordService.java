package com.sky.phone.service;

public interface PhoneRecordService<T> {
    T publishPhoneRecords();
}
