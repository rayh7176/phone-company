package com.sky.phone.component.impl;

import com.sky.phone.component.PhoneRecordParser;
import com.sky.phone.data.CustomerPhoneRecords;
import com.sky.phone.data.PhoneCall;
import com.sky.phone.data.PhoneRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PhoneRecordParserImpl implements PhoneRecordParser<Map<CharSequence, List<PhoneCall>>, CustomerPhoneRecords> {

    public static final String REGEX_POINTS = ":";
    Logger logger = LoggerFactory.getLogger(PhoneRecordParserImpl.class);

    private final long FIXED_THREE_MINUTES_DURATION = 180;

    private final double CALL_COST = 0.05;

    private final double EXTRA_COST_CALL = 0.03;

    public CustomerPhoneRecords sumPhoneRecords (Map<CharSequence, List<PhoneCall>> phoneInfo){
        CustomerPhoneRecords.Builder customerRecords = CustomerPhoneRecords.newBuilder();
        List<PhoneRecord> phoneRecords = new ArrayList<>();

        // sum results
        for (Map.Entry<CharSequence, List<PhoneCall>> recordEntry : phoneInfo.entrySet()) {
            Map<String, List<PhoneCallDetails>>
                    phoneCallDetails = recordEntry.getValue().stream().map(
                            s ->  {
                                // need to calculate cost per phone call
                                long duration = Duration.ofSeconds(Arrays.stream(Objects.toString(s.getDuration()).split(REGEX_POINTS))
                                        .mapToInt(n -> Integer.parseInt(n))
                                        .reduce(0, (n, m) -> n * 60 + m)).getSeconds();
                                // calculate cost based on
                                return new PhoneCallDetails(Objects.toString(s.getPhoneNumber()),getPhoneCallCosts(duration));
                            }
                    ).sorted(Comparator.comparing(PhoneCallDetails::getCost).reversed()).
                    collect(Collectors.groupingBy(PhoneCallDetails::getPhoneNumber ));

            // compare cost of calls
            double maxValue = compareCostOfCalls(phoneCallDetails);

            double totalCost = phoneCallDetails.values().stream().flatMap(
                    p -> p.stream().map( c -> c.getCost())).reduce(0d, (n,m) -> n + m) - maxValue;


            PhoneRecord.Builder builder = PhoneRecord.newBuilder();
            phoneRecords.add(builder.setCustomerId(recordEntry.getKey())
                    .setDuration(String.format("%.2f", totalCost)).build());
            if (logger.isDebugEnabled())
                logger.info("print time key="+recordEntry.getKey() +", value="+totalCost);
        }
        customerRecords.setPhoneRecords(phoneRecords);
        return customerRecords.build();
    }

    /**
     * there is a promotion on and the calls made to the phone
     * number with the greatest total cost is removed from the customer's bill.
     * @param phoneCallDetails
     * @return
     */
    private double compareCostOfCalls(Map<String, List<PhoneCallDetails>> phoneCallDetails) {
        double maxValue = 0;
        for (Map.Entry<String, List<PhoneCallDetails>> entry : phoneCallDetails.entrySet()){
            double cost = entry.getValue().get(0).getCost();
            if ( cost > maxValue ) {
                maxValue = cost;
            }
        }
        return maxValue;
    }

    /**
     * For a customer the cost of a call up to and including 3 minutes in duration is charged at 0.05p/sec,
     * any call over 3 minutes in duration the additional time is charged at 0.03p/sec. However,
     * @param duration
     * @return
     */
    private Double getPhoneCallCosts(long duration) {
        double costCall;
        //Up to 3 min 0.05
        if (duration <= FIXED_THREE_MINUTES_DURATION) {
            costCall = duration * CALL_COST;
        } else {
            //longer duration
            long tmpDuration = duration - FIXED_THREE_MINUTES_DURATION;
            costCall = tmpDuration * EXTRA_COST_CALL + FIXED_THREE_MINUTES_DURATION * CALL_COST;
        }
        return costCall;
    }

    /**
     * Phone details holder
     */
    private class PhoneCallDetails implements Comparable<Double>{

        private final String phoneNumber;
        private final Double cost;

        PhoneCallDetails(String number, Double cost) {
            this.phoneNumber = number;
            this.cost = cost;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public Double getCost() {
            return cost;
        }

        @Override
        public int compareTo(Double price) {
            return cost.compareTo(price);
        }

        @Override
        public String toString() {
            return "PhoneCallDetails{" +
                    "phoneNumber='" + phoneNumber + '\'' +
                    ", cost=" + cost +
                    '}';
        }
    }
}
