package com.sky.phone.component.impl;

import com.sky.phone.component.PhoneRecordsReader;
import com.sky.phone.data.PhoneCall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PhoneRecordsReaderImpl implements PhoneRecordsReader<Map<CharSequence, List<PhoneCall>>> {

    Logger logger = LoggerFactory.getLogger(PhoneRecordsReaderImpl.class);

    public static final String New_LINE = "\n";

    public static final String EMPTY_SPACE = "\\s";

    public Map<CharSequence, List<PhoneCall>> parsePhoneRecords(String fileName){
        try {
            File file = ResourceUtils.getFile("classpath:"+fileName);
            //Read File Content
            String content = new String(Files.readAllBytes(file.toPath()));
            String [] records = content.split(New_LINE);
            return Arrays.stream(records).
                    map( s -> {
                                if (logger.isDebugEnabled())
                                    logger.info("Phone details retrieved, record="+s);
                                List<String> newLine = Arrays.asList(s.split(EMPTY_SPACE));
                                PhoneCall.Builder builder = PhoneCall.newBuilder();
                                if (newLine.size() == 3) {
                                    builder.setCustomerId(newLine.get(0)).
                                            setPhoneNumber(newLine.get(1)).
                                            setDuration(newLine.get(2));
                                    return builder.build();
                                } else {
                                    return new PhoneCall();
                                }
                            }
                        ).collect(Collectors.groupingBy(PhoneCall::getCustomerId));
        } catch (FileNotFoundException e) {
            throw new RuntimeException("File Not found", e);
        } catch (IOException e) {
            throw new RuntimeException("IOException ",e);
        }
    }
}
