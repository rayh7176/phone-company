package com.sky.phone.component;

public interface PhoneRecordsReader<T> {
    T parsePhoneRecords(String fileName);
}
