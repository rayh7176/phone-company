package com.sky.phone.component;

public interface PhoneRecordParser<E,R> {
    R sumPhoneRecords(E e);
}
