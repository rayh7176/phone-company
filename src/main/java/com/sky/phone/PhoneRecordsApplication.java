package com.sky.phone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhoneRecordsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhoneRecordsApplication.class, args);
	}

}
