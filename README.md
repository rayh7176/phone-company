# Phone Company

For the purpose of this exercise the results are displayed in the web site

@Required.
- Java 8.
- mvn

In order to run the application you need to do the following

1- mvn clean install

2- mvn spring-boot:run

wait for Spring boot app to start, from the browser

3-  http://localhost:8090/phone/records

results:

Should be similar to the below

{"PhoneRecords":[{"customerId":"A","duration":"37.04"},{"customerId":"B","duration":"30.63"}]}

I created two helpers Avro records that are generated as required

PhoneCall = `'customer id','phone number called','call duration'`

CustomerPhoneRecords contains [ PhoneRecord = `'customer id','call duration'` ]

Note.
Due to the time restriction, I only added the main test to show the flow.

